#!/usr/bin/env bash

#  main.command
#  Création d'une liste numérotée de dossiers

#  Created by Gamel Sylvain on 09/02/2012.
#  Copyright (c) 2012 S.G. inTech. All rights reserved.

# Vérifie que le dossier destination existe bien et que c'est un dossier
echo 1>&2 "Destination=$destinationFolder"

if [ ! -d "$destinationFolder" ]
then
    echo "Destination folder '$destinationFolder' is not a valid directory" 1>&2
    exit 1;
fi

# Change le répertoire courant vers le dossier destination
cd "$destinationFolder"

# Boucle autant que nécessaire pour créer les dossiers
for (( idx=1 ; idx<=$nbFolders ; idx++ ))
do
    export newName='x'
    export numberStr=$idx

    # Construit le numéro en utilisant le bon format
    if [ 1 == "$fixedDigits" ]
    then
        numberStr=$( printf "%0*d" $nbOfDigits $idx )
    fi

    # Place le numéro au bon endroit
    if [ "1" == "$numberPos" ]
    then
        newName=$numberStr"$name"
    else
        newName="$name"$numberStr
    fi

    # Créé le dossier et affiche son chemin (pour alimenter la sortie)
    mkdir "$newName"
    echo "$(pwd)/$newName"
done
